<!DOCTYPE html>
<html style="font-size: 16px;" lang="es"><head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="keywords" content="Regitro Nacional de Víctimas">
    <meta name="description" content="">
    <title>Inicio</title>
    <link rel="stylesheet" href="nicepage.css" media="screen">
<link rel="stylesheet" href="Casa.css" media="screen">
<link rel="stylesheet" href="EstilosI2.css">
    <meta name="generator" content="Nicepage 4.17.10, nicepage.com">
    <link id="u-theme-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Open+Sans:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i">
    <link id="u-page-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i">
    
    
    <meta name="theme-color" content="#478ac9">
    <meta property="og:title" content="Casa">
    <meta property="og:type" content="website">
  </head>
  <body data-home-page="Casa.html" data-home-page-title="Casa" class="u-body u-xl-mode" data-lang="es">
  
    <section class="u-align-center u-clearfix u-valign-top u-section-1" id="carousel_bbf3">
      
      <div class="u-expanded-width u-palette-3-base u-shape u-shape-rectangle u-shape-1">
        <ul class="menu">
          <li><a href="Formulario.php">Dar de alta una víctima</a></li>
          <li><a href="#">Editar un formulario</a></li>
          <li><a href="#">Dar de baja una víctima</a></li>
      </ul>
      <br><br><br><br><br><br><br><br><br><br>
      <h2 class="u-custom-font u-font-montserrat u-text u-text-body-alt-color u-text-default u-text-1">
        <br>Regitro Nacional de Víctimas
      </h2>
      </div>
      <div class="u-list u-list-1"></div>
    </section>
    
    <div class="cont">
      <div>
      <h3>¿Qué es el Registro Nacional de Víctimas?</h3>
      <br><br>
          <p>Si eres víctima de un delito o de violación a derechos humanos, esta información te será de utilidad.</p>
          <br>
          <p> La Ley General de Víctimas establece en su capítulo IV de los apartados del Sistema Nacional de 
              Atención a Víctimas que el Registro Nacional de Víctimas es la unidad administrativa de la Comisión 
              Ejecutiva encargada de llevar y salvaguardar el padrón de víctimas a nivel nacional e inscribir los 
              atos de las víctimas del delito y de violaciones a derechos humanos del orden federal. </p>
      </div>
      <video controls src="./media/videoP.mp4" class="vid"></video>
      </div>
    
    <footer class="u-align-center u-clearfix u-footer u-grey-80 u-footer" id="sec-5d47"><div class="u-clearfix u-sheet u-sheet-1">
        <p class="u-small-text u-text u-text-variant u-text-1">Texto de muestra. Haz click para seleccionar el elemento de Texto.</p>
      </div></footer>
    <section class="u-backlink u-clearfix u-grey-80">
      <a class="u-link" href="https://nicepage.com/website-templates" target="_blank">
        <span>Website Templates</span>
      </a>
      <p class="u-text">
        <span>created with</span>
      </p>
      <a class="u-link" href="" target="_blank">
        <span>Website Builder Software</span>
      </a>. 
    </section>
  
</body></html>